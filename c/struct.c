#include <stdio.h>

// Create a position object
typedef struct Position {
	double x;
	double y;
} Position;

int main() {
	// Declaring a struct variable
	Position position;
	
	// Assign values to struct variable
	position.x = 1.0;
	position.y = 3.0;
	return 0;
}
