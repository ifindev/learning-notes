# Learning Notes
This repo is a compilation of programming learning notes made by me. The reason I made this repo is due to a lifelong headache of mine when learning some new programming concepts. The process usually goes like this:

- Learn a new concept. Example, `union inside struct` concept in C.
- Write a code to make the concept clear. Then tweak the code as much as I can to fulfill my own curiosity. 
-  Understood the concept. Save the code somewhere in my laptop. Close the file. Never open it again, until...
- Found a problem which include the concepts I had learned before. Then going through all the files in my PC just to find myself finally annoyed because the code and all the explanation was now missing. 
- Reread all the things while calming myself. Hahaha. 

That is why with the help of this repo, I hope that the problem I am facing today will not repeat again in the next future. 

## Update 14th October 2020

### Work Condition
Anyway, as per 14th October of 2020, I am now an RF Engineer, or a Software Engineer to be exact, developing a program and hardware for Local Positioning System (LPS) for autonomous vehicle and robot using DWM1000 module. I might write about it later in the future, but maybe in a private repository since all my works seemed to become company's property. 

Because of that, I am now dealing mostly with C and C++ for programming Arduino and later maybe Raspberry Pi to implement a more advanced Algorithm for a very accurate positioning results. I am thinking about implementing a Kalman Filter for Sensor Fusion technique using DWM1000 module, accelerometer sensor, compass sensor, and probably rotary encoder. Since learning the theory only would not be fun, I will definitely write a lot of code to implement the Kalman filter either in C, C++, or Python. But I really hope I can implement it in C. 

### Personal Target
Even though I am really grateful for the work I have right now, I still am have a dream to become a very good Full Stack Web Developer, the Top 10 best programmer in Asia if I may. This is the list of things I want to learn right now:
- HTML5 (Learn from Traversy Media Course. Better just read the HTML code and learn from it. [HTML5 Course](https://www.youtube.com/watch?v=UB1O30fR-EE&list=PLillGF-RfqbYeckUaD1z6nviTp31GLTH8&index=1)
- [CSS3 course](https://www.youtube.com/watch?v=yfoY53QXEnI&list=PLillGF-RfqbYeckUaD1z6nviTp31GLTH8&index=2)
- CSS Flexbox and Grid
- Vanilla Javascript and DOM manipulations. 
- Deploying a simple HTML,CSS, and JS site to vercel or heroku
- Learn to use Docker
- React JS for front end
- NodeJS for backend
- HTTP Crash course
- Database
And, that's about it for now!

